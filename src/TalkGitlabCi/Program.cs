﻿using System;

namespace TalkGitlabCi
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Calculadora.Somar(5, 5));
            Console.WriteLine(Calculadora.Subtrair(10, 5));
            Console.WriteLine(Calculadora.Multiplicar(2, 6));
            Console.WriteLine(Calculadora.Dividir(14, 2));
        }
    }
}
