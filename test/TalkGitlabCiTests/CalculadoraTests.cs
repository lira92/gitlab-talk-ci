using TalkGitlabCi;
using Xunit;

namespace TalkGitlabCiTests
{
    public class CalculadoraTests
    {
        [Fact]
        public void TestarSoma()
        {
            Assert.Equal(10, Calculadora.Somar(5, 5));
        }

        [Fact]
        public void TestarSubtracao()
        {
            Assert.Equal(2, Calculadora.Subtrair(7, 5));
        }

        [Fact]
        public void TestarMultiplicacao()
        {
            Assert.Equal(12, Calculadora.Multiplicar(2, 6));
        }

        [Fact]
        public void TestarDivisao()
        {
            Assert.Equal(2, Calculadora.Dividir(6, 3));
        }
    }
}